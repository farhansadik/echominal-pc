#!/bin/bash
#!/usr/bin/env bash

set import bash
set import busybox
set import dialog
set import nmcli
set import data_set=[popUp, gala]

####################################################################
# Farhan Sadik - December 2019
####################################################################

# Define variables
BLUE='\033[1;34m'; RED='\033[1;31m'; YELLOW='\033[1;33m'; CYAN='\033[1;36m'; 
deep_green='\033[0;32m'; GREEN='\033[1;92m'; WHITE='\033[1;97m';
STOP='\033[0m';  # stop 
BLINK='\033[5m'; # blink

# project 
script_version="0.2.001-${RED}01${STOP}"                          # pre 0.2.001-01
# engine 
engine_version="${BLINK}0.0.010-${RED}02 Alpha${STOP}"            # pre 0.002-03 Alpha


function getInput() {
	# for test - new algorithm
	local var="$@"
	echo && read -p "${var}" input; echo 
}
function shell() {

	# implementing 
	# echo; echo -n "`cat $shell_style`" && read input; echo; 
	echo; echo -n "admin@example ~> " && read shell; echo;

}
# Invalid Input Function
error() {
	echo &&  echo -e "${RED}${BLINK} *** Invalid choice or input *** "${NC} && echo && exit
}
export -f error

#Force using ROOT
force_root () {
	if [[ "$UID" -ne 0 ]]; then {
		echo && echo -e "${YELLOW}Sorry, you need to run this as ${RED}'root'" ${NC} && echo && exit;
	}; fi
}
export -f force_root
# Purpose: Display pause prompt
function pause(){
	local message="$@"
	[ -z $message ] && message="Press [Enter] key to continue..."
	read -p "$message" readEnterKey
}
function write_header(){
	local h="$@"
	echo "------------------------------------------------------";
	echo " ${h}";
	echo "------------------------------------------------------";
}
checkServices() {
	# experimantal 
	# pacman - arch
	if systemctl list-unit-files --state=enabled > /tmp/s2.txt; then {
		dialog \
			--title "List of enabled services" \
			--textbox /tmp/s2.txt 22 70;
			clear 
	}; else {
		printf "failed to read data of services!\n"; 
	}; fi;
}
function systemUpdate() {
	
	echo && sleep 0.5; # clear ; # add
	figlet "System Upgrade"; 
	printf "\n${RED}Checking for updates..\n${STOP}";
	
	# pacman - arch 
	if pacman -Sy ; then { 
		printf "${BLUE}Update Compleate\n${STOP}" 
	}; else { 
		printf "failed to update, please try again\n"
	}; fi;

	function update() {
		# pacman - arch 
		printf "\n${YELLOW}Preparing for system upgrade.....${STOP}\n";
		if pacman -Syyu; then { 
			printf "${BLUE}System upgrade has been compleated\n${STOP}"
		}; else { 
			printf "failed to upgrade, please try again\n"
		}; fi;
	}
	
	pause
}

# install a package
function installPackage() {
	# pacman - arch 
	getInput "enter package name ~> "
	#echo && read -p "enter package name ~> " package_name
	if pacman -S $input; then printf "${YELLOW}Install Compleated\n${STOP}"; fi; echo; pause;
}

# uninstall a package
function uninstallPackage() {
	# pacman - arch 
	getInput "enter package name ~> "
	echo; if pacman -R $input; then printf "${YELLOW}Uninstall Compleated\n${STOP}"then ; fi;
	pause
}
# search a package 
function searchPackage() {
	# pacman - arch 
	# experimental
	getInput "enter package name ~> ";
	if pacman -Q | grep $package_name; then {
		printf "${BLUE}Package Found\n${STOP}";
	}; else {
		printf "${BLUE}Package is not found\n${STOP}";
	}; fi
	pause
}
function removePackageCache() {
	# pacman - arch 
	# experimantal 
	printf "${BLUE}Cleaning Cache${STOP}";
	if pacman -Scc; then printf "${BLUE}Operation Success\n${STOP}"; fi; pause
}
# this function will provide info inside of your device 
function diskUsage() {
	# root required 
	
	# modificatrion required

	usep=$(echo $output | awk '{ print $1}' | cut -d'%' -f1)
	  partition=$(echo $output | awk '{print $2}')
	write_header " Disk Usage Info"
	if [ "$EXCLUDE_LIST" != "" ] ; then
	  df -H | grep -vE "^Filesystem|tmpfs|cdrom|${EXCLUDE_LIST}" | awk '{print $5 " " $6}'
	  #df -H | grep -vE "^Filesystem|tmpfs|cdrom|${EXCLUDE_LIST}" | awk '{print $5 " " $6}'
	else
	  df -H | grep -vE "^Filesystem|tmpfs|cdrom" | awk '{print $5 " " $6}'
	  #df -H | grep -vE "^Filesystem|tmpfs|cdrom" | awk '{print $5 " " $6}'
	fi; pause	
}
function networkDevice() {
	# experimantal 
	# pacman - arch
	if nmcli device show > /tmp/network_device.txt; then {
		dialog \
			--title "Network Device Information" \
			--textbox /tmp/network_device.txt 0 0; # 22 70 
			clear 
	}; else {
		printf "failed to read data of data!\n"; 
	}; fi; 
}
function about() {

	# implemented from echominal-mobile

	printf " $YELLOW
EchoMinal Engine
Termux Manager for$deep_green Android OS $YELLOW

Engine Version   : $engine_version $YELLOW
Script Version   : $script_version $YELLOW
Termux           : Termux v0.74
Linux Kernel     : `uname -sr`

EchoMinal gives you a simple way to access
termux application.

contact to dev ~ farhansadikriduan@gmail.com
blogspot ~ https://squaredevops.blogspot.com

Farhan Sadik
Square Development Group
$STOP \n"; pause
}
function main(){

	# implementing 
	# for test 

	# force_root && echo 
	printf "EchoMinal PC$RED $engine_version";
	echo # should be removed letter 
	echo -e "${YELLOW} ${BLINK}
    ######################################################################
    #            _        _    _    _    _     _    __     __            #
    #           | |      | |  | \  | |  | |   | |   \ \   / /            #
    #           | |      | |  |  \ | |  | |   | |    \ \_/ /             #
    #           | |      | |  |   \| |  | |   | |    / /_\ \             #
    #           | |___   | |  | |\   |  | |___| |   / /   \ \            #
    #           |_____|  |_|  |_| \__|  |_______|  /_/     \_\           #
    #                                                                    #
    ###################################################################### "${STOP} && echo

	#clear && printf "$red" && figlet "EchoMinal"; printf "$stop"; 
	#printf "$deep_green" && printf "Termux Manager for Android OS\n"; printf "\n$stop"

	# printing menu
    echo -e "${CYAN}        BASIC${NC}                                 ${CYAN}SYSTEM${NC}                           ${CYAN}OTHERS"${NC}
    printf "\n$YELLOW"
    echo -e          "1.  view enabled services           10.  System Upgrade                  19.  ***"
    echo -e          "2.  ***                             11.  Install Package                 20.  ***"
    echo -e          "3.  ***                             12.  Remove\Uninstall Package        21.  Check IP"
    echo -e          "4.  ***                             13.  Search Package                  22.  About"
    echo -e          "5.  ***                             14.  Remove Package Cache            00.  Exit"
    echo -e          "6.  ***                             15.  Operating System Info"
    echo -e          "7.  ***                             16.  Disk Usage"
    echo -e          "8.  ***                             17.  EchoMinal Settings"
    echo -e          "9.  ***                             18.  View Network Device"${STOP}
	echo

	# input method
	echo -e -n "${BLUE}main@menu ~> $STOP" && read c

	# case statement 
	case $c in
		1) checkServices ;;
		2) empty_slot ;;
		3) empty_slot ;;
		4) empty_slot ;;
		5) empty_slot ;;
		6) empty_slot ;;
		7) empty_slot ;;
		8) empty_slot ;;
		9) empty_slot ;;
		10) systemUpdate ;;
		11) installPackage ;;
		12) uninstallPackage ;;
		13) searchPackage ;;
		14) removePackageCache ;;
		15) empty_slot ;; # will have do it in dialog 
		16) diskUsage ;;
		17) empty_slot ;; # echominal settings
		18) networkDevice ;;
		19) empty_slot ;;
		20) empty_slot ;;
		21) empty_slot ;;
		22) about ;;
		00) echo && footer ;; # exit 
		*)
			echo "Please select between 1 to 5 choice only.";
			printf "Sorry you've entered wrong options\n"; sleep 1;
			echo -e "\e[5mPlease Wait.....\e[0m"; sleep 0.5
			echo -e "$RED\e[7mRebooting Engine.....\e[0m" && sleep 0.5;
			# $engine;
	# pause
	esac

}
function footer() {
	# printf "$deep_green" && 
	#printf "${WHITE}EchoMinal ${YELLOW}$script_version\n${WHITE}Created by Farhan Sadik\n $stop";
	printf "${WHITE}EchoMinal ${YELLOW}$script_version\n${WHITE}Development Branch\n $stop";
	echo && exit 0;
}

# main logic
while true
	do
		#show_menu # display memu
		main # wait for user input
done

# A Program By, Farhan Sadik
# Copyright (C) Square Development Group